//
//  PlayerCell.swift
//  KatalogKosarkasa
//
//  Created by uros.rupar on 5/28/21.
//

import UIKit

class PlayerCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        profilePicture.layer.borderWidth = 3
        profilePicture.layer.masksToBounds = false
        profilePicture.layer.borderColor = UIColor.white.cgColor
        profilePicture.layer.cornerRadius = profilePicture.frame.width * 0.5
        profilePicture.clipsToBounds = true
        
        slider.minimumValue = 130;
        slider.maximumValue = 250;
        
        
        makeLabelDesign(firstname)
        makeLabelDesign(lastname)
        makeLabelDesign(age)
        makeLabelDesign(height)
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBOutlet weak var profilePicture: UIImageView!
    
    @IBOutlet weak var firstname: UILabel!
    
    
    @IBOutlet weak var lastname: UILabel!
    
    @IBOutlet weak var age: UILabel!
    
    
    @IBOutlet weak var slider: UISlider!
    
    @IBOutlet weak var height: UILabel!
    
    
    @IBAction func sliderChangevalue(_ sender: Any) {
        
        height.text = String(Int(slider.value))
    }
    
    func makeLabelDesign(_ label:UILabel){
        label.layer.borderWidth = 3;
        label.layer.borderColor = CGColor(red: 255, green: 255, blue: 255, alpha: 1.0)
        label.layer.cornerRadius = 20
        
        label.backgroundColor = .red
        
        label.layer.masksToBounds = true

    }
}
