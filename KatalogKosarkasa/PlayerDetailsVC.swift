//
//  PlayerDetailsVC.swift
//  KatalogKosarkasa
//
//  Created by uros.rupar on 5/27/21.
//

import UIKit

class PlayerDetailsVC: UIViewController {

    var currentPlayer = Player(name: "", surename: "", height: 0.0, age: 0, image: "")
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        profileImage.image = UIImage(named: currentPlayer.image)
        age.text = String(currentPlayer.age)
        height.text = String(currentPlayer.height)
        
        godineFixedLabel.textColor = UIColor(displayP3Red: 0, green: 64, blue: 200, alpha: 1.0)
        
        visinaFixedLabel.textColor = UIColor(displayP3Red: 0, green: 64, blue: 200, alpha: 1.0)
        
      makeLabelDesign(age)
        makeLabelDesign(height)
    }
    

    @IBOutlet weak var godineFixedLabel: UILabel!
    @IBOutlet weak var visinaFixedLabel: UILabel!
    
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var height: UILabel!
    
    func makeLabelDesign(_ label:UILabel){
        label.layer.borderWidth = 3;
        label.layer.borderColor = CGColor(red: 255, green: 255, blue: 255, alpha: 1.0)
        label.layer.cornerRadius = 20
        
        label.backgroundColor = .red
        
        label.layer.masksToBounds = true

    }
}
