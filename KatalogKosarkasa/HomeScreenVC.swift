//
//  HomeScreenVC.swift
//  KatalogKosarkasa
//
//  Created by uros.rupar on 5/29/21.
//

import UIKit

class HomeScreenVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        buttonTeam.layer.borderWidth = 3;
        buttonTeam.layer.borderColor = CGColor(red: 0, green: 64, blue: 221, alpha: 1.0)
        buttonTeam.layer.cornerRadius = 20
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBOutlet weak var buttonTeam: UIButton!
    
    

    
}
