//
//  ViewController.swift
//  KatalogKosarkasa
//
//  Created by uros.rupar on 5/27/21.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    

    let fileName = "Player"
    let fileXstension = "csv"
    
    var fileContent : String?
    
    var players:[Player] = []
    
    @IBOutlet weak var playerTable: UITableView!
    
  
    
 
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return players.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as!  PlayerCell
        
        cell.profilePicture.image = UIImage(named: players[indexPath.row].image)
        cell.firstname.text = players[indexPath.row].name
        cell.lastname.text = players[indexPath.row].surename
        cell.age.text = String(players[indexPath.row].age)
        cell.height.text = String(players[indexPath.row].height)
        //cell.slider.setValue(Float(players[indexPath.row].height), animated: true)
        cell.slider.value = Float(players[indexPath.row].height)
       
        
        return cell
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PlayerDetails"{
            let controller = segue.destination as! PlayerDetailsVC
            controller.currentPlayer = players[self.playerTable.indexPathForSelectedRow!.row]
            segue.destination.title = " \(players[self.playerTable.indexPathForSelectedRow!.row].name) \(players[self.playerTable.indexPathForSelectedRow!.row].surename)"
        }
    }
    

  
    
    func parseToModel(){
        
        let path = Bundle.main.path(forResource: fileName, ofType: fileXstension)
        
        fileContent = try? String(contentsOfFile: path!, encoding: String.Encoding.utf8)
        if let fileContent = self.fileContent{
            
           
            
            let lineByLineArray = fileContent.split(separator: "\n")
            
           
            for row in lineByLineArray{
                let lineToArray = row.split(separator: ",")
                
                
                let name = String(lineToArray[0])
                
                let surename = String(lineToArray[1])
                let height = Double(lineToArray[3]) ?? 0.0
                let age = Int(lineToArray[2]) ?? 0
                let image = String(lineToArray[4])
            
               
                let player = Player(name: name, surename: surename, height: height, age: age,image: image)
               
                                         
                
                players.append(player)
            }
        }
    }
    
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        parseToModel()
        
        
    }

}

