//
//  Player.swift
//  KatalogKosarkasa
//
//  Created by uros.rupar on 5/28/21.
//

import Foundation
struct Player {
    let name:String
    let surename: String
    let height: Double
    let age: Int
    let image:String
}
